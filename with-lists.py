def encryptWithLists(text, key):
    listText = []
    listKey = []
    encryptedText = ''
    for it in key:
        listKey.append((ord(it.upper()) - 65))
    for it in text:
        listText.append((ord(it.upper()) - 65))
        
    for i in range(0, len(listText)):
        encryptedText += chr((listText[i] + listKey[i%len(listKey)])%26 + 65)
    return encryptedText

def decryptWithLists(text, key):
    listText = []
    listKey = []
    decryptedText = ''
    for it in key:
        listKey.append((ord(it.upper()) - 65))
    for it in text:
        listText.append((ord(it.upper()) - 65))
        
    for i in range(0, len(listText)):
        num = (listText[i] - listKey[i%len(listKey)])
        if num < 0:
            decryptedText += chr(num + 91)
        else:
            decryptedText += chr(num + 65)
    return decryptedText
    
    
    
def selfInvokin():
    actionType = input('Type "de" for decryption sequence,\n"en" - for encryption\n')
    if actionType != "de" and actionType != "en" :
        print("Wrong format !")
        return
    text = input('\n Input text: \n')
    key = input('\n Input key: \n')
    if actionType == "de":
        print('\n', decryptWithLists(text, key))
    else:
        print('\n', encryptWithLists(text, key))
    
    
selfInvokin()
    