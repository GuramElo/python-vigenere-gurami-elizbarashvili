def encryptWithStrings(text, key):
    text = text.upper()
    key = key.upper()
    encryptedText = ''
    for i in range(0, len(text)):
        encryptedText += chr(((ord(text[i])-65) + ord(key[i%len(key)])-65)%26 + 65)
    return encryptedText

def decryptWithStrings(text, key):
    text = text.upper()
    key = key.upper()
    decryptedText = ''
    for i in range(0, len(text)):
        num = (ord(text[i]) - ord(key[i%len(key)]))
        if num < 0:
            decryptedText += chr(num + 91)
        else:
            decryptedText += chr(num + 65)
    return decryptedText
    
    
    
def selfInvokin():
    actionType = input('Type "de" for decryption sequence,\n"en" - for encryption\n')
    if actionType != "de" and actionType != "en" :
        print("Wrong format !")
        return
    text = input('\n Input text: \n')
    key = input('\n Input key: \n')
    if actionType == "de":
        print('\n', decryptWithStrings(text, key))
    else:
        print('\n', encryptWithStrings(text, key))
    
    
selfInvokin()