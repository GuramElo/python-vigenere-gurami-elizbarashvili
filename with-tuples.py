def encryptWithTuples(text, key):
    wupText = ()
    wupKey = ()
    encryptedText = ''
    for it in key:
        wupKey += (ord(it.upper()) - 65),
        
    for it in text:
        wupText += (ord(it.upper()) - 65),
        
    for i in range(0, len(wupText)):
        encryptedText += chr((wupText[i] + wupKey[i%len(wupKey)])%26 + 65)
    return encryptedText

def decryptWithTuples(text, key):
    wupText = ()
    wupKey = ()
    decryptedText = ''
    for it in key:
        wupKey += (ord(it.upper()) - 65),
    for it in text:
        wupText += (ord(it.upper()) - 65),
        
    for i in range(0, len(text)):
        num = (wupText[i] - wupKey[i%len(wupKey)])
        if num < 0:
            decryptedText += chr(num + 91)
        else:
            decryptedText += chr(num + 65)
    return decryptedText
    
    
def selfInvokin():
    actionType = input('Type "de" for decryption sequence,\n"en" - for encryption\n')
    if actionType != "de" and actionType != "en" :
        print("Wrong format !")
        return
    text = input('\n Input text: \n')
    key = input('\n Input key: \n')
    if actionType == "de":
        print('\n', decryptWithTuples(text, key))
    else:
        print('\n', encryptWithTuples(text, key))
    
    
selfInvokin()